package com.example.practica3ig

import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.practica3ig.ui.TimePickerFragment
import kotlinx.android.synthetic.main.activity_picker.*

class PickerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)
    }

    fun showTimePickerDialog(v: View) {
       // val timePickerFragment = TimePickerFragment()
        val timerPickerFragment = TimePickerFragment.newInstance(TimePickerDialog.OnTimeSetListener
        { view, hourOfDay, minute ->
            pkrTime.setText("${hourOfDay}:${minute}")
        })

        timerPickerFragment.show(supportFragmentManager, "dataPicker")

    }
}